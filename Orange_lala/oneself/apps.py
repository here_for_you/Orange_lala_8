from django.apps import AppConfig


class OneselfConfig(AppConfig):
    name = 'oneself'
