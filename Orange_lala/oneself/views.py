from django.shortcuts import render

# Create your views here.

#个人中心
def myself_index(request):
    return render(request, "person/index.html")

#个人资料
#个人信息
def myself_information(request):
    return render(request, "person/information.html")

#安全设置
def myself_safety(request):
    return render(request, "person/safety.html")

#支付密码
def myself_setpay(request):
    return render(request, "person/setpay.html")

#修改密码
def myself_password(request):
    return render(request, "person/password.html")

#绑定手机
def myself_bindphone(request):
    return render(request, "person/bindphone.html")

#绑定邮箱
def myself_email(request):
    return render(request, "person/email.html")

#实名认证
def myself_idcard(request):
    return render(request, "person/idcard.html")

#设置安全问题
def myself_question(request):
    return render(request, "person/question.html")

#收货地址
def myself_address(request):
    return render(request, "person/address.html")

#我的交易
#订单管理
def myself_order(request):
    return render(request, "person/order.html")

#订单详情
def myself_orderinfo(request):
    return render(request, "person/orderinfo.html")

#退换货
def myself_refund(request):
    return render(request, "person/refund.html")

#查看物流
def myself_logistics(request):
    return render(request, "person/logistics.html")

#发表评价
def myself_commentlist(request):
    return render(request, "person/commentlist.html")

#退款售后
def myself_change(request):
    return render(request, "person/change.html")

#钱款去向
def myself_record(request):
    return render(request, "person/record.html")

#我的资产
#优惠券
def myself_coupon(request):
    return render(request, "person/coupon.html")

#红包
def myself_bonus(request):
    return render(request, "person/bonus.html")

#账单明细
def myself_bill(request):
    return render(request, "person/bill.html")

def myself_billlist(request):
    return render(request, "person/billlist.html")

#我的小屋
#收藏
def myself_collection(request):
    return render(request, "person/collection.html")

#足迹
def myself_foot(request):
    return render(request, "person/foot.html")

#评价
def myself_comment(request):
    return render(request, "person/comment.html")

#消息
def myself_news(request):
    return render(request, "person/news.html")

#消息详情
def myself_blog(request):
    return render(request, "person/blog.html")

