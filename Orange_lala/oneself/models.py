# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AllTrade(models.Model):
    clname = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'all_trade'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class BaseArea(models.Model):
    base_areaid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    parentid = models.PositiveIntegerField()
    vieworder = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        db_table = 'base_area'


class BrandHome(models.Model):
    nick = models.CharField(db_column='Nick', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'brand_home'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('User', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Goodclass(models.Model):
    good_class_name = models.CharField(max_length=255)
    all_trade = models.ForeignKey(AllTrade, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'goodclass'


class Goods(models.Model):
    image = models.CharField(max_length=100)
    name = models.CharField(unique=True, max_length=255)
    brand = models.CharField(max_length=255)
    pack = models.CharField(max_length=255)
    is_sugar = models.CharField(max_length=255)
    taste = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    specification = models.CharField(max_length=255)
    materials = models.CharField(max_length=255)
    production_certificate = models.CharField(max_length=255)
    storage_method = models.CharField(max_length=255)
    expiration_date = models.CharField(max_length=255)
    import_domestic = models.CharField(max_length=255)
    weight = models.CharField(max_length=255)
    product_standard = models.CharField(db_column='Product_standard', max_length=255)  # Field name made lowercase.
    repertory = models.IntegerField()
    all_trade = models.ForeignKey(AllTrade, models.DO_NOTHING)
    brand_home = models.ForeignKey(BrandHome, models.DO_NOTHING)
    good_class = models.ForeignKey(Goodclass, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'goods'


class GroupH(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey('User', models.DO_NOTHING)
    user_member = models.ForeignKey('UserMenber', models.DO_NOTHING, db_column='user_member')

    class Meta:
        managed = False
        db_table = 'group_h'


class HomeLun(models.Model):
    lun_id = models.AutoField(primary_key=True)
    lun_name = models.CharField(max_length=255)
    lun_image = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'home_lun'


class Shopcart(models.Model):
    good_image = models.TextField()
    good_name = models.CharField(max_length=255)
    good_color = models.CharField(max_length=255)
    good_pack = models.CharField(max_length=255)
    good_price = models.FloatField()
    good_count = models.ForeignKey(Goods, models.DO_NOTHING, db_column='good_count')

    class Meta:
        managed = False
        db_table = 'shopcart'


class User(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField(blank=True, null=True)
    username = models.CharField(unique=True, max_length=150, blank=True, null=True)
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=150, blank=True, null=True)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField(blank=True, null=True)
    is_active = models.IntegerField(blank=True, null=True)
    date_joined = models.DateTimeField(blank=True, null=True)
    nickname = models.CharField(max_length=255, blank=True, null=True)
    sex = models.CharField(max_length=1, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    phone = models.CharField(max_length=11)
    address = models.CharField(max_length=255, blank=True, null=True)
    birth = models.DateField(blank=True, null=True)
    idcard = models.CharField(max_length=18, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user'


class UserGroups(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_groups'
        unique_together = (('user', 'group'),)


class UserMenber(models.Model):
    rember = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'user_menber'


class UserUserPermissions(models.Model):
    user = models.ForeignKey(User, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_user_permissions'
        unique_together = (('user', 'permission'),)
