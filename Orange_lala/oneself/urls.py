from django.urls import path
from oneself import views

app_name = 'myself'

urlpatterns = [
    path('myself/', views.myself_index, name='index'),
    path('myself/myself_information/', views.myself_information, name='myself_information'),
    path('myself/myself_safety/', views.myself_safety, name='myself_safety'),
    path('myself/myself_setpay/', views.myself_setpay, name='myself_setpay'),
    path('myself/myself_password/', views.myself_password, name='myself_password'),
    path('myself/myself_bindphone/', views.myself_bindphone, name='myself_bindphone'),
    path('myself/myself_email/', views.myself_email, name='myself_email'),
    path('myself/myself_idcard/', views.myself_idcard, name='myself_idcard'),
    path('myself/myself_question/', views.myself_question, name='myself_question'),
    path('myself/address/', views.myself_address, name='address'),
    path('myself/myself_order/', views.myself_order, name='myself_order'),
    path('myself/myself_orderinfo/', views.myself_orderinfo, name='myself_orderinfo'),
    path('myself/myself_refund/', views.myself_refund, name='myself_refund'),
    path('myself/myself_logistics/', views.myself_logistics, name='myself_logistics'),
    path('myself/myself_commentlist/', views.myself_commentlist, name='myself_commentlist'),
    path('myself/myself_change/', views.myself_change, name='myself_change'),
    path('myself/myself_record/', views.myself_record, name='myself_record'),
    path('myself/myself_coupon/', views.myself_coupon, name='myself_coupon'),
    path('myself/myself_bonus/', views.myself_bonus, name='myself_bonus'),
    path('myself/myself_bill/', views.myself_bill, name='myself_bill'),
    path('myself/myself_billlist/', views.myself_billlist, name='myself_billlist'),
    path('myself/myself_collection/', views.myself_collection, name='myself_collection'),
    path('myself/myself_foot/', views.myself_foot, name='myself_foot'),
    path('myself/myself_comment/', views.myself_comment, name='myself_comment'),
    path('myself/myself_news/', views.myself_news, name='myself_news'),
    path('myself/myself_blog/', views.myself_blog, name='myself_blog'),
    ]