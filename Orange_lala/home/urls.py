from django.urls import path
from home import views

app_name = "home"

urlpatterns = [
    path('home/', views.homepage, name='homepage'),
    path('home/register/', views.register, name='register'),
    path('home/login/', views.login, name='login'),
    path('home/sort/', views.sort, name='sort'),
    path('home/introduction/', views.introduction, name='introduction'),
    path('home/search/', views.search, name='search'),
    path('home/shopcart/', views.shopcart, name='shopcart'),
    path('home/pay/', views.pay, name='pay'),
    path('home/success/', views.success, name='success'),
]